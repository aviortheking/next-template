import React from 'react'
import { Text } from '@dzeio/components'

export default class Homepage extends React.Component {

	public render = (): JSX.Element => (
		<main>
			<Text>Hello World !</Text>
		</main>
	)

}
